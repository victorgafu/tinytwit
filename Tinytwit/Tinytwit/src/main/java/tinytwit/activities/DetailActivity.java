package tinytwit.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import tinytwit.data.DBTweet;
import tinytwit.data.Tweet;
import tinytwit.tinitwit.utilities.NetworkReceiver;
import tinytwit.ui.SimpleToast;

/**
 * This class shows the detail of a saved tweet at the database
 *
 * @author Victor Galvez
 *
 */

public class DetailActivity extends Activity {
    private static final String TAG = "TweetDetail";

    // Database
    DBTweet db = new DBTweet(this);

    // UI
    ImageView imageViewProfile;
    TextView textViewName;
    TextView textViewTweetText;
    TextView textViewFavoriteCount;
    TextView textViewRetweetCount;
    TextView textViewPubDate;
    WebView web;
    ImageView imageViewPicture;

    private SimpleToast toaster;
    private NetworkReceiver receiver;
    private Tweet tweet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initWidgets();
        obtainTweet();
        loadTweet();
        loadProfileImage();

        // The web only will be loaded if the tweet doesn't have an image
        if (!loadImage()) {
            loadWeb();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    /**
     * Obtain all the widget references
     */
    private void initWidgets() {
        receiver = new NetworkReceiver(this);

        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewTweetText = (TextView) findViewById(R.id.textViewTweetText);
        textViewFavoriteCount = (TextView) findViewById(R.id.textViewFavoriteCount);
        textViewRetweetCount = (TextView) findViewById(R.id.textViewRetweetCount);
        textViewPubDate = (TextView) findViewById(R.id.textViewPubDate);
        web = (WebView) findViewById(R.id.webViewDetail);
        imageViewPicture = (ImageView) findViewById(R.id.imageViewPicture);

        // Prepare the WebView to accept the shortened url from the tweets.
        web.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
    }
    /**
     * Obtain the correspondent tweet
     */
    private void obtainTweet() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            // Obtain the Tweet id
            Long id = extras.getLong("id");

            // Obtain the correspondent tweet from the database
            try {
                db.open();
                tweet = db.obtainTweet(id);
                db.close();
            } catch (Exception e) {
                toaster.toast(R.string.error_db);
            }
        }
    }
    /**
     * Loads the data tweet information at the widgets of the activity
     */
    private void loadTweet() {
        // If the tweet is not found.
        if (tweet == null) {
            toaster.toast(R.string.tweet_not_found);
            return;
        }

        textViewName.setText(tweet.name);
        textViewTweetText.setText(tweet.text);
        textViewFavoriteCount.setText(tweet.favorites + "");
        textViewRetweetCount.setText(tweet.retweets + "");
        textViewPubDate.setText(tweet.publicationDate.toString());
        setTitle("@" + tweet.idUser);
    }

    /**
     * If the tweet contains an image load it at the ImageView and do it Visible.
     * If there's not hide the ImageView
     *
     * @return true if the image is visible false if is hidden.
     */
    private boolean loadImage() {
        // Check if the tweet have an image.
        if (tweet.media != null && tweet.media.length() > 0) {
            imageViewPicture.setVisibility(View.VISIBLE);
        } else {
            imageViewPicture.setVisibility(View.GONE);
            return false;
        }

        // Try to load the image.
        try {
            String filename = tweet.media;
            File file = new File(getFilesDir() + MainActivity.MEDIA_FOLDER,
                    filename);
            Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
            imageViewPicture.setImageBitmap(image);
            return true;
        } catch (Exception e) {
            // If there's an error return false.
            return false;
        }
    }
    /**
     * Load the linked web at the tweet if there's any and if we have connection. If there's
     * not connection shows the default message
     *
     */
    private void loadWeb() {
        // Check if the tweet have an URL
        if (tweet.url != null) {
            web.setVisibility(View.VISIBLE);
        } else {
            // If not, hide the WebView and return
            web.setVisibility(View.GONE);
            return;
        }

        // Show the WebView
        if (receiver.checkSilentConnetion()) {
            web.loadUrl(tweet.url.toString());
        } else {
            web.loadUrl("file:///android_asset/offline.html");
        }
    }
    /**
     * Load the profile image
     */
    private void loadProfileImage() {
        // Check if the profile image exists
        if (tweet.photo == null || tweet.photo.length() == 0) {
            // If not returns
            return;
        }

        String filename = tweet.photo;
        File file = new File(getFilesDir() + MainActivity.PROFILES_FOLDER,
                filename);
        Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
        imageViewProfile.setImageBitmap(image);
    }
}
